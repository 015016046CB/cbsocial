<?php
   $servername ="localhost";
   $username = "root";
   $password = "";
   $database = "asesorias";
     $conexion = mysqli_connect($servername, $username, $password, $database)
   or die("Error en la conexión");
   
   $consulta=mysqli_query($conexion,"SELECT matricula,nombre,apellido_paterno,apellido_materno,carrera,id_grupo from alumno")
   or die ("Error al traer los datos");

   ?>
<html>
	<head>
		<title>Alumnos</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<script src="js/jquery.min.js"></script>
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="#">Ver Alumnos</a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
			<nav id="menu">
				<ul class="links">
					<li><a href="index.html">Inicio</a></li>
					<li><a href="#">Alumnos</a></li>
					<li><a href="vermaestro.php">Maestros</a></li>
					<li><a href="verasesoria.php">Asesorias</a></li>
				</ul>
			</nav>

		<!-- Main -->
			<div id="main">
				<section class="wrapper style1">
					<div class="inner">

						<header class="align-center">
							<h2 id="content">Alumnos Registrados</h2>
						</header>
						<hr class="major" />
						
						<form class="form-inline" role="search" id="buscar">
  							<a data-toggle="modal" href="#newModal" class="button alt">Agregar</a>
    					</form>

    					<div class="modal fade" id="newModal" tabindex="-1" role="dialog" aria-labelledby="newModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title">Agregar</h4>
        </div>
        
        <div class="modal-body">
<form role="form" method="post" id="agregar">
	<div class="form-group">
    <label for="nombre">Matricula</label>
    <input type="text" class="form-control" name="nombre" required>
  </div>
  <div class="form-group">
    <label for="nombre">Nombre</label>
    <input type="text" class="form-control" name="nombre" required>
  </div>
  <div class="form-group">
    <label for="sexo">Apellido Materno</label>
    <select name="sexo" class="form-control">
    </select>
  </div>
  <div class="form-group">
    <label for="edad">Apellido Paterno</label>
    <input type="text" class="form-control" name="edad" required>
  </div>
  <div class="form-group">
    <label for="telefono">Carrera</label>
    <input type="text" class="form-control" name="telefono" >
  </div>
  <div class="form-group">
    <label for="direccion">Grupo</label>
    <input type="text" class="form-control" name="direccion" >
  </div>

  <button type="submit" class="btn btn-default">Agregar</button>
</form>
        </div>
							<div class="table-wrapper">
								<table class="alt">
									<thead>
										<tr>
											<th>Matricula</th>
											<th>Nombre</th>
											<th>Apellido Paterno</th>
											<th>Apellido Materno</th>
											<th>Carrera</th>
											<th>Grupo</th>
											<th>Eliminar</th>
											<th>Modificar</th>
										</tr>
									</thead>
									<tbody>
										<?php 
        									while($extraido=mysqli_fetch_array($consulta)){
       	 								?>
            							<tr>
							                <td><?php echo $extraido["matricula"]?></td>
							                <td><?php echo $extraido["nombre"]?></td>
							                <td><?php echo $extraido["apellido_paterno"]?></td>
							                <td><?php echo $extraido["apellido_materno"]?></td>
							                <td><?php echo $extraido["carrera"]?></td>
							                <td><?php echo $extraido["id_grupo"]?></td>
							                <td><a class="button fit" href="#">Eliminar</a></td>
							                <td><a class="button alt fit" href="#">Modificar</a></td>
							            </tr>
            							<?php   
								        }
								     ?>
									</tbody>
								</table>
							</div>
					</div>
				</section>
			</div>

		<!-- Footer -->
			<footer id="footer">
				<div class="copyright">
					<ul class="icons">
						<li><a href="https://twitter.com/upqoficial" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="https://www.facebook.com/UPQoficial/" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="https://www.instagram.com/soyupq/" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
					</ul>
					<p>&copy; By Quique & Faby :v All rights reserved.</p>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>