<?php
   $servername ="localhost";
   $username = "root";
   $password = "";
   $database = "asesorias";
     $conexion = mysqli_connect($servername, $username, $password, $database)
   or die("Error en la conexión");
   
   $consulta=mysqli_query($conexion,"SELECT asesoria.materia,asesoria.hora,asesoria.dia,asesoria.aula,maestro.nombre,maestro.apellido_paterno ,asesoria.carrera, asesoria.cupo
   	from asesoria INNER JOIN maestro where asesoria.id_maestro=maestro.id_maestro")
   or die ("Error al traer los datos");

   ?>
<html>
	<head>
		<title>Asesorias</title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body class="subpage">

		<!-- Header -->
			<header id="header">
				<div class="logo"><a href="#">Ver Asesorias</a></div>
				<a href="#menu">Menu</a>
			</header>

		<!-- Nav -->
			<nav id="menu">
				<ul class="links">
					<li><a href="index.html">Inicio</a></li>
					<li><a href="veralumno.php">Alumnos</a></li>
					<li><a href="vermaestro.php">Maestros</a></li>
					<li><a href="#">Asesorias</a></li>
				</ul>
			</nav>

		<!-- Main -->
			<div id="main">
				<section class="wrapper style1">
					<div class="inner">

						<header class="align-center">
							<h2 id="content">Asesorias registradas</h2>
						</header>
						<hr class="major" />
							<div class="table-wrapper">
								<table class="alt">
									<thead>
										<tr>
											<th>Materia</th>
											<th>Hora</th>
											<th>D&iacute;a</th>
											<th>Aula</th>
											<th>Nombre</th>
											<th>Apellido</th>
											<th>Carrera</th>
											<th>Cupo</th>
											<th>Eliminar</th>
											<th>Modificar</th>
										</tr>
									</thead>
									<tbody>
										<?php 
        									while($extraido=mysqli_fetch_array($consulta)){
       	 								?>
            							<tr>
							                <td><?php echo $extraido["materia"]?></td>
							                <td><?php echo $extraido["hora"]?></td>
							                <td><?php echo $extraido["dia"]?></td>
							                <td><?php echo $extraido["aula"]?></td>
							                <td><?php echo $extraido["nombre"]?></td>
							                <td><?php echo $extraido["apellido_paterno"]?></td>
							                <td><?php echo $extraido["carrera"]?></td>
							                <td><?php echo $extraido["cupo"]?></td>
							                <td><a class="button fit" href="#">Eliminar</a></td>
							                <td><a class="button alt fit" href="#">Modificar</a></td>
							            </tr>
            							<?php   
								        }
								     ?>
									</tbody>
								</table>
							</div>
					</div>
				</section>
			</div>

		<!-- Footer -->
			<footer id="footer">
				<div class="copyright">
					<ul class="icons">
						<li><a href="https://twitter.com/upqoficial" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
						<li><a href="https://www.facebook.com/UPQoficial/" class="icon fa-facebook"><span class="label">Facebook</span></a></li>
						<li><a href="https://www.instagram.com/soyupq/" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
					</ul>
					<p>&copy; By Quique & Faby :v All rights reserved.</p>
				</div>
			</footer>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/jquery.scrolly.min.js"></script>
			<script src="assets/js/jquery.scrollex.min.js"></script>
			<script src="assets/js/skel.min.js"></script>
			<script src="assets/js/util.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>